import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 200,
    },
  });

class InputBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.value || ''
        };
    }

    static getDerivedStateFromProps(nextProps, nextStates) {
        return {
            value: nextStates.value
        };
    }

    onChange = (e) => {
        this.setState({
            value: e.target.value
        }, () => {
            this.props.onChange(this.state.value);
        });
    }

    render() {
        const { label, name, classes } = this.props;

        return (
            <div className={`field-block ${name}`}>
                <TextField
                    id={name}
                    label={label}
                    className={classes.textField}
                    value={this.state.value}
                    onChange={e => this.onChange(e)}
                />
            </div>
        );
    }
}

InputBox.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(InputBox);