import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    button: {
      margin: theme.spacing.unit,
    },
});
  
function ActionButton({ classes, label, onClick, color }) {
    return (
        <Button
            variant="contained"
            className={classes.button}
            color={color}
            onClick={onClick}>
            {label}
        </Button>
    );
}

ActionButton.propTypes = {
    classes: PropTypes.object.isRequired,
    color: PropTypes.string,
};
  
export default withStyles(styles)(ActionButton);